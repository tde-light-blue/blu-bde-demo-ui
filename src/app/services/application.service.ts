import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';
import { Application } from '../interfaces/application.interface';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  constructor(private http: HttpClient) { }

  getApplications(): Observable<Application[]> {
    return this.http.get<Application[]>(environment.applicationEndpoint);
  }

  getApplicationById(applicationId: string): Observable<Application> {
    return this.http.get<Application>(`${environment.applicationEndpoint}/${applicationId}`);
  }

  upsertApplication(application: Application) {
    return this.http.post<Application>(environment.applicationEndpoint, application);
  }

  deleteApplicationById(applicationId: string): Observable<void> {
    return this.http.delete<void>(`${environment.applicationEndpoint}/${applicationId}`);
  }
}
