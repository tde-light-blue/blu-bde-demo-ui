import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Application } from '../interfaces/application.interface';
import { ApplicationService } from '../services/application.service';
import { ApplicationErrorStateMatcher } from './error_state_matcher';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  applicationId: string | null = "";
  application?: Application;

  statusOptions: string[] = [
    "open",
    "confirmed",
    "in progress",
    "done",
    "cancelled"
  ];

  teamOptions: string[] = [
    "Blue",
    "Light Blue",
    "Green",
    "Red",
    "Light Red"
  ];

  applicationFormGroup = new FormGroup({
    applicationId: new FormControl('', Validators.required),
    applicationName: new FormControl('', Validators.required),
    migrationStatus: new FormControl('', Validators.required),
    responsiblePerson: new FormControl('', Validators.required),
    responsibleTeamTDE: new FormControl('', Validators.required),
  })
  errorStateMatcher = new ApplicationErrorStateMatcher();

  constructor(private route: ActivatedRoute, private applicationService: ApplicationService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.applicationId = params.get('id');
      if (this.applicationId) {
        this.applicationService.getApplicationById(this.applicationId)
          .subscribe(application => this._setInitialApplication(application));
      } else {
        this.applicationFormGroup.controls.applicationId.validator = null;
      }
    });
  }

  onSubmit() {
    if (this.applicationFormGroup.valid) {
      const application: Application = {
        applicationId: this.applicationId ? this.applicationFormGroup.controls.applicationId.value : null,
        applicationName: this.applicationFormGroup.controls.applicationName.value ?? "",
        migrationStatus: this.applicationFormGroup.controls.migrationStatus.value ?? "",
        responsiblePerson: this.applicationFormGroup.controls.responsiblePerson.value ?? "",
        responsibleTeamTDE: this.applicationFormGroup.controls.responsibleTeamTDE.value ?? "",
      }

      this.applicationService.upsertApplication(application).subscribe();
    }
  }

  private _setInitialApplication(application: Application): void {
    this.application = application;
    this.applicationFormGroup.setValue({
      applicationId: application.applicationId,
      applicationName: application.applicationName,
      migrationStatus: application.migrationStatus,
      responsiblePerson: application.responsiblePerson,
      responsibleTeamTDE: application.responsibleTeamTDE,
    })
  }
}
