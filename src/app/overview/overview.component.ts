import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { Application } from '../interfaces/application.interface';
import { ApplicationService } from '../services/application.service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit, AfterViewInit {

  dataSource = new MatTableDataSource<Application>();
  displayedColumns = ["action", "id", "name", "status", "responsible", "teamTDE"];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild("filterInput") inputName: any;

  constructor(private applicationService: ApplicationService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.applicationService.getApplications()
      .subscribe(applications => this.dataSource.data = applications);

    this.dataSource.filterPredicate = this._filterByApplication();
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  hasData(): boolean {
    return this.dataSource.data != null && this.dataSource.data.length > 0;
  }

  editApplication(application: Application) {
  }

  deleteApplication(application: Application) {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: {
        confirmationText: `Do you really want to delete the application "${application.applicationName}"?`,
      }
    })
      .afterClosed()
      .subscribe(deleteApplication => {
        console.log("Result of Delete Application " + deleteApplication);
        if (deleteApplication) {
          this.applicationService.deleteApplicationById(application.applicationId!)
            .subscribe();
          this.dataSource.data = this.dataSource.data
            .filter(dataSourceApplication => dataSourceApplication.applicationId !== application.applicationId);
        }
      });
  }

  clearFilter() {
    this.dataSource.filter = "";
    this.inputName.nativeElement.value = "";
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  private _filterByApplication(): any {
    return (data: Application, filter: string): boolean => {
      if (!filter) {
        return false;
      }
      return data.applicationName.trim().toLowerCase().startsWith(filter);
    }
  }
}
