import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { DetailsComponent } from './details/details.component';
import { OverviewComponent } from './overview/overview.component';
import { UserAuthenticationGuard } from './shared/user-authentication.guard';
import { UnauthenticatedComponent } from './unauthenticated/unauthenticated.component';

const routes: Routes = [
  { path: 'overview', component: OverviewComponent, canActivate: [UserAuthenticationGuard] },
  { path: 'edit/:id', component: DetailsComponent, canActivate: [UserAuthenticationGuard] },
  { path: 'create', component: DetailsComponent, canActivate: [UserAuthenticationGuard] },
  { path: 'unauthenticated', component: UnauthenticatedComponent, },
  { path: '', redirectTo: 'overview', pathMatch: 'full' },
  { path: 'overview', redirectTo: 'overview', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
