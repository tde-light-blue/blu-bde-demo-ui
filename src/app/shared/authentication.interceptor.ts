import { HttpInterceptor, HttpRequest, HttpEvent, HttpHandler } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';

@Injectable()
export class HttpAuthenticationInterceptor implements HttpInterceptor {
	constructor(private oAuthService: OAuthService) { }
	// NOT in use/active
	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		request = request.clone({
			setHeaders: {
				Authorization: 'Bearer ' + this.oAuthService.getAccessToken()
			}
		});
		return next.handle(request);
	}
}
