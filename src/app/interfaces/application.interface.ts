export interface Application {
    applicationId: string | null;
    applicationName: string;
    responsiblePerson: string;
    migrationStatus: string;
    responsibleTeamTDE: string;
}