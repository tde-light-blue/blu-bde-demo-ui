# Blu BDE Demo Ui

Small frontend service written in Typescript with Angular that serves for demo and presentation purposes.

## Functionality

The service provides a GUI for the corresponding Blu BDE demo backend service
